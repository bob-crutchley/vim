## My VIM Configurations
Run this command to install:
```bash
git clone https://gitlab.com/bob-crutchley/vim && cd vim && ./install
```
This will overwrite `~/.vimrc` and `~/.vim`

You will need to have the `curl` tool installed for this installation to work.  
Change `set bg=light` to be `set bg=dark` in the `vimrc` file if you have a dark terminal, which is usually the case.


### Plugins
- **[vim-polyglot](https://github.com/sheerun/vim-polyglot)**  
	A collection of language packs for Vim
- **[nerdtree](https://github.com/scrooloose/nerdtree)**  
	A tree explorer plugin for vim
- **[vim-gradle](https://github.com/tfnico/vim-gradle)**  
	This vim bundle simply recognizes .gradle files as being groovy syntax
- **[vim-groovy](https://github.com/thecodesmith/vim-groovy)**  
	This plugin is a collection of useful features for working in the Groovy language
- **[vim-surround](https://github.com/tpope/vim-surround)**  
	Surround.vim is all about "surroundings": parentheses, brackets, quotes, XML tags, 
	and more. The plugin provides mappings to easily delete, change and add such surroundings in pairs.
- **[vim-fugitive](https://github.com/tpope/vim-fugitive)**  
	A Git wrapper so awesome, it should be illegal
- **[supertab](https://github.com/ervandew/supertab)**  
	Perform all your vim insert mode completions with Tab
- **[nerdtree-git-plugin](https://github.com/xuyuanp/nerdtree-git-plugin)**  
	A plugin of NERDTree showing git status
- **[vim-airline](https://github.com/vim-airline/vim-airline)**  
	Lean & mean status/tabline for vim that's light as air

