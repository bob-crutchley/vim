" set language to English 
set spelllang=en

" spell file, containing user defined words
set spellfile=$HOME/.vim/en.utf-8.add

" tab is 4 spaces set tabstop=4 " colours to suite light terminal theme
set background=dark

" show line numbers
set number

" enable syntax highlighting
syntax on

" toggle spell check with Ctrl + D
map <c-d> :setlocal spell!<cr>

" stop terminal bell sounds
set noeb vb t_vb=

" turn autoindentations
set autoindent
filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab

" Leader Key
let mapleader = ","

" Navigating with guides
inoremap <leader><leader> <Esc>/<++><Enter>"_c4l
vnoremap <leader><leader> <Esc>/<++><Enter>"_c4l
map <leader><leader> <Esc>/<++><Enter>"_c4l

" remappings
imap jj <Esc>
map <C-E> :Explore<Enter>

" Generic
autocmd FileType html,markdown,latex inoremap <Leader>snl <Esc>/\.<Space>[A-Z]<Enter>ls<Enter>
autocmd FileType html,markdown,latex inoremap <Leader>csnl <Esc>/,<Space><Enter>s.<Esc>ls<Enter><Esc>gUli
map <Leader>sh <Esc>:silent<Space>!<Space>${SHELL}<Enter><C-L>

" HTML
autocmd FileType html inoremap <Leader>code <code></code><Esc>F<i<Enter><Enter><Esc>ki
autocmd FileType html map <Leader>code i<code></code><Esc>F<i<Enter><Enter><Esc>ki
autocmd FileType html inoremap <Leader>.code <code></code><Esc>F<i
autocmd FileType html map <Leader>.code i<code></code><Esc>F<i
autocmd FileType html inoremap <Leader>ul <ul></ul><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html map <Leader>ul i<ul></ul><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html inoremap <Leader>.ul <ul></ul><Esc>F<i
autocmd FileType html map <Leader>.ul i<ul></ul><Esc>F<i
autocmd FileType html inoremap <Leader>ol <ol></ol><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html map <Leader>ol i<ol></ol><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html inoremap <Leader>.ol <ol></ol><Esc>F<i
autocmd FileType html map <Leader>.ol i<ol></ol><Esc>F<i
autocmd FileType html inoremap <Leader>li <li></li><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html map <Leader>li i<li></li><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html inoremap <Leader>.li <li></li><Esc>F<i
autocmd FileType html map <Leader>.li i<li></li><Esc>F<i
autocmd FileType html inoremap <Leader>div <div></div><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html map <Leader>div i<div></div><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html inoremap <Leader>.div <div></div><Esc>F<i
autocmd FileType html map <Leader>.div i<div></div><Esc>F<i
autocmd FileType html inoremap <Leader>h1 <h1></h1><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html map <Leader>h1 i<h1></h1><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html inoremap <Leader>.h1 <h1></h1><Esc>F<i
autocmd FileType html map <Leader>.h1 i<h1></h1><Esc>F<i
autocmd FileType html inoremap <Leader>h2 <h2></h2><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html map <Leader>h2 i<h2></h2><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html inoremap <Leader>.h2 <h2></h2><Esc>F<i
autocmd FileType html map <Leader>.h2 i<h2></h2><Esc>F<i
autocmd FileType html inoremap <Leader>h3 <h3></h3><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html map <Leader>h3 i<h3></h3><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html inoremap <Leader>.h3 <h3></h3><Esc>F<i
autocmd FileType html map <Leader>.h3 i<h3></h3><Esc>F<i
autocmd FileType html inoremap <Leader>h4 <h4></h4><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html map <Leader>h4 i<h4></h4><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html inoremap <Leader>.h4 <h4></h4><Esc>F<i
autocmd FileType html map <Leader>.h4 i<h4></h4><Esc>F<i
autocmd FileType html inoremap <Leader>p <p></p><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html map <Leader>p i<p></p><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html inoremap <Leader>.p <p></p><Esc>F<i
autocmd FileType html map <Leader>.p i<p></p><Esc>F<i
autocmd FileType html inoremap <Leader>head <head></head><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html map <Leader>head i<head></head><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html inoremap <Leader>.head <head></head><Esc>F<i
autocmd FileType html map <Leader>.head i<head></head><Esc>F<i
autocmd FileType html inoremap <Leader>body <body></body><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html map <Leader>body i<body></body><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html inoremap <Leader>.body <body></body><Esc>F<i
autocmd FileType html map <Leader>.body i<body></body><Esc>F<i
autocmd FileType html inoremap <Leader>em <em></em><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html map <Leader>em i<em></em><Esc><Esc>F<i<Enter><Esc>O<Tab>
autocmd FileType html inoremap <Leader>.em <em></em><Esc>F<i
autocmd FileType html map <Leader>.em i<em></em><Esc>F<i

" GoLang
autocmd FileType go inoremap <Leader>fun <Esc>Ifunc<Space><Esc>A(<++>)<Space><++><Space>{<Enter>}<Esc>O<Tab><++><Esc>k^f(i
autocmd FileType go map <Leader>fun <Esc>Ifunc<Space><Esc>A(<++>)<Space><++><Space>{<Enter>}<Esc>O<Tab><++><Esc>k^f(i
autocmd FileType go inoremap <Leader>println fmt.Println()<Esc>F)i
autocmd FileType go map <Leader>println fmt.Println()<Esc>F)i
autocmd FileType go inoremap <Leader>iferr if<Space>err<Space>!=<Space>nil<Space>{<Enter>}<Esc>O<Tab> autocmd FileType go inoremap <Leader>import import<Space>(<Enter>)<Esc>O<Tab>""<Esc>i 
autocmd FileType go map <Leader>iferr if<Space>err<Space>!=<Space>nil<Space>{<Enter>}<Esc>O<Tab> autocmd FileType go map <Leader>import import<Space>(<Enter>)<Esc>O<Tab>""<Esc>i 

" Java
autocmd FileType java inoremap <Leader>psvm public<Space>static<Space>void<Space>main(String[]<Space>args)<Space>{<Enter>}<Esc>O<Tab>
autocmd FileType java map <Leader>psvm public<Space>static<Space>void<Space>main(String[]<Space>args)<Space>{<Enter>}<Esc>O<Tab>
autocmd FileType java inoremap <Leader>fun <Esc>Ipublic<Space>static<Space>void<Space><Esc>A()<Space>{<Enter>}<Esc>O<Tab>
autocmd FileType java map <Leader>fun <Esc>Ipublic<Space>static<Space>void<Space><Esc>A()<Space>{<Enter>}<Esc>O<Tab>
autocmd FileType java inoremap <Leader>println System.out.println();<Esc>F)i
autocmd FileType java map <Leader>println System.out.println();<Esc>F)i
autocmd FileType java inoremap <Leader>class <Esc>Iclass<Space><Esc>A<Space>{<Enter>}<Esc>O<Tab>
autocmd FileType java map <Leader>class <Esc>Iclass<Space><Esc>A<Space>{<Enter>}<Esc>O<Tab>
autocmd FileType java inoremap <Leader>pclass <Esc>Ipublic<Space>class<Space><Esc>A<Space>{<Enter>}<Esc>O<Tab>
autocmd FileType java map <Leader>pclass <Esc>Ipublic<Space>class<Space><Esc>A<Space>{<Enter>}<Esc>O<Tab>

" Python
autocmd FileType python map <Leader>lint       <Esc>:! python3 -m pylint %<Enter>
autocmd FileType python inoremap <Leader>fun def<Space><++>(<++>):<Enter><Tab><++><Esc>k^
autocmd FileType python map <Leader>fun def<Space><++>(<++>):<Enter><Tab><++><Esc>k^

" NGINX
autocmd FileType nginx inoremap <Leader>server server<Space>{<Enter>}<Esc>O<Tab>
autocmd FileType nginx map <Leader>server server<Space>{<Enter>}<Esc>O<Tab>
autocmd FileType nginx inoremap <Leader>location <Esc>Ilocation<Space><Esc>A<Space>{<Enter>}<Esc>O<Tab>
autocmd FileType nginx map <Leader>location <Esc>Ilocation<Space><Esc>A<Space>{<Enter>}<Esc>O<Tab>

" Javascript
autocmd FileType javascript inoremap <Leader>afun ()<Space>=><Space>{<Enter>}<Esc>O<Tab><++><Esc>k^f)i
autocmd FileType javascript map <Leader>afun ()<Space>=><Space>{<Enter>}<Esc>O<Tab><++><Esc>k^f)i
autocmd FileType javascript inoremap <Leader>cl console.log()<Esc>i
autocmd FileType javascript map <Leader>cl console.log()<Esc>i
autocmd FileType javascript inoremap <Leader>fun function<Space>(<++>)<Space>{<Enter>}<Esc>O<++><Esc>k^f(i
autocmd FileType javascript map <Leader>fun function<Space>(<++>)<Space>{<Enter>}<Esc>O<++><Esc>k^f(i
autocmd FileType javascript inoremap <Leader>me module.exports
autocmd FileType javascript map <Leader>me module.exports
autocmd FileType javascript inoremap <Leader>fe forEach()<Esc>i
autocmd FileType javascript map <Leader>fe forEach()<Esc>i
autocmd FileType javascript inoremap <Leader>forin for<Space>(<Space>in<Space><++>)<Space>{<Enter>}<Esc>O<Tab><++><Esc>k^f(a
autocmd FileType javascript map <Leader>forin for<Space>(<Space>in<Space><++>)<Space>{<Enter>}<Esc>O<Tab><++><Esc>k^f(a
autocmd FileType javascript inoremap <Leader>while while<Space>()<Space>{<Enter>}<Esc>O<Tab><++><Esc>k^f)i
autocmd FileType javascript map <Leader>while while<Space>()<Space>{<Enter>}<Esc>O<Tab><++><Esc>k^f)i


